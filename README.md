# Strobeshnik

Displaying text using persistence of vision effect.

This variant is optimized for cost and build simplicity, but as a result has a bit more complicated software.
Instead of using position sensor for synchronization this variant relies on the DC motor current consumption
(which depends on the commutator).

## Pictures

![Device](doc/img-device.jpg)

![Display in dark](doc/img-display-night.jpg)
![Display at daylight](doc/img-display-day.jpg)

![Algorithm graph](doc/plot.png)

## Documentation

### Manuals

Sorry, these are croatian only.

 * [Build Instructions](doc/Upute%20za%20izradu.pdf)
 * [Operating Manual](doc/Upute%20za%20kori%C5%A1tenje.pdf)
 * [Presentation](doc/Prezentacija.pdf)


### Hardware

[Schematic](hw/POV-sch.pdf)
