#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "ir.h"


/*
 * Note:
 *   This is adopted from https://github.com/z3t0/Arduino-IRremote
 *   All credits go to those authors!
 */

#define USEC_PER_TICK	100		// Microseconds per clock interrupt tick
#define RAWBUF			40		// Maximum length of raw duration buffer
#define MARK			false
#define SPACE			true
#define _GAP			5000	// Minimum gap between IR transmissions
#define GAP_TICKS		(_GAP/USEC_PER_TICK)
#define TRIG_TICKS		((1600+470)/2/USEC_PER_TICK)
#define KEY(x)			((uint8_t)~(x) | ((uint16_t)(x) << 8))

static volatile enum {
	STATE_IDLE = 0,
	STATE_MARK,
	STATE_SPACE,
	STATE_STOP,
	STATE_OVERFLOW,
} rcvstate;

static volatile uint16_t timer;
static volatile uint8_t rawlen;
static volatile uint16_t rawdata;


void ir_start(void) {
	// Setup pulse clock timer interrupt
	// Prescale /8 (8M/8 = 1 microseconds per tick)
	// Therefore, the timer interval can range from 1 to 256 microseconds
	OCR0A  = (uint64_t)F_CPU * USEC_PER_TICK / 8 / 1000000;
	TCCR0A = (2 << WGM00);
	TCCR0B = (2 << CS00);
	TIMSK0 = _BV(OCIE0A);

	rcvstate = STATE_IDLE;
	rawdata = 0;
	rawlen = 0;
}

static char decode_key(void) {
	switch (rawdata) {
#if REMOTE_VARIANT == 0
		case KEY(0x98): return KEY_0;
		case KEY(0xA2): return KEY_1;
		case KEY(0x62): return KEY_2;
		case KEY(0xE2): return KEY_3;
		case KEY(0x22): return KEY_4;
		case KEY(0x02): return KEY_5;
		case KEY(0xC2): return KEY_6;
		case KEY(0xE0): return KEY_7;
		case KEY(0xA8): return KEY_8;
		case KEY(0x90): return KEY_9;
		case KEY(0x68): return KEY_STAR;
		case KEY(0xB0): return KEY_HASH;
		case KEY(0x18): return KEY_UP;
		case KEY(0x4A): return KEY_DOWN;
		case KEY(0x10): return KEY_LEFT;
		case KEY(0x5A): return KEY_RIGHT;
		case KEY(0x38): return KEY_OK;
#elif REMOTE_VARIANT == 1
		case KEY(0x4A): return KEY_0;
		case KEY(0x68): return KEY_1;
		case KEY(0x98): return KEY_2;
		case KEY(0xB0): return KEY_3;
		case KEY(0x30): return KEY_4;
		case KEY(0x18): return KEY_5;
		case KEY(0x7A): return KEY_6;
		case KEY(0x10): return KEY_7;
		case KEY(0x38): return KEY_8;
		case KEY(0x5A): return KEY_9;
		case KEY(0x42): return KEY_STAR;
		case KEY(0x52): return KEY_HASH;
		case KEY(0x62): return KEY_UP;
		case KEY(0xA8): return KEY_DOWN;
		case KEY(0x22): return KEY_LEFT;
		case KEY(0xC2): return KEY_RIGHT;
		case KEY(0x02): return KEY_OK;
#else
#	error Unknown REMOTE_VARIANT!
#endif
	}

	return KEY_NONE;
}

char ir_read(void) {
	char key = KEY_NONE;
	if (rcvstate == STATE_STOP) {
		key = decode_key();
		rawlen = 0;
		rawdata = 0;
		rcvstate = STATE_IDLE;
	} else if (rcvstate == STATE_OVERFLOW) {
		rawlen = 0;
		rcvstate = STATE_IDLE;
	}
	return key;
}

//+=============================================================================
// Interrupt Service Routine - Fires every USEC_PER_TICK
// TIMER0 interrupt code to collect raw data.
// Widths of alternating SPACE, MARK are recorded in rawbuf.
// Recorded in ticks of USEC_PER_TICK [microseconds]
// 'rawlen' counts the number of entries recorded so far.
// First entry is the SPACE between transmissions.
// As soon as a the first [SPACE] entry gets long:
//   Ready is set; State switches to IDLE; Timing of SPACE continues.
// As soon as first MARK arrives:
//   Gap width is recorded; Ready is cleared; New logging starts
//
#pragma GCC push_options
#pragma GCC optimize ("O2")
ISR(TIM0_COMPA_vect) {
	// Read if IR Receiver -> SPACE [xmt LED off] or a MARK [xmt LED on]
	bool irdata = bit_is_set(PINB, PB1);

	++timer;	// One more tick
	if (rawlen >= RAWBUF) rcvstate = STATE_OVERFLOW;

	switch (rcvstate) {
		case STATE_IDLE:	// In the middle of a gap
			if (irdata == MARK) {
				if (timer < GAP_TICKS) {	// Not big enough to be a gap
					timer = 0;
				} else {
					// Gap just ended; Record duration; Start recording transmission
					rawlen = 0;
					timer = 0;
					rcvstate = STATE_MARK;
				}
			}
			break;

		case STATE_MARK:	// Timing Mark
			if (irdata == SPACE) {	// Mark ended; Record time
				timer = 0;
				rcvstate = STATE_SPACE;
			}
			break;

		case STATE_SPACE:	// Timing Space
			if (irdata == MARK) {	// Space just ended; Record time
				if (rawlen >= 17 && rawlen <= 32) {
					bool bit = timer > TRIG_TICKS;
					rawdata = (rawdata << 1) | bit;
				}
				++rawlen;
				timer = 0;
				rcvstate = STATE_MARK;
			} else if (timer > GAP_TICKS) {	// Space
				// A long Space, indicates gap between codes
				// Flag the current code as ready for processing
				// Switch to STOP
				// Don't reset timer; keep counting Space width
				rcvstate = STATE_STOP;
			}
			break;

		case STATE_OVERFLOW:	// Flag up a read overflow; Stop the State Machine
		case STATE_STOP:	// Waiting; Measuring Gap
			if (irdata == MARK) {
				timer = 0;	// Reset gap timer
			}
			break;
	}
}
#pragma GCC pop_options
