#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>


#define SYM_FIRST	' '
#define SYM_LAST	127
#define SYM_WIDTH	5
#define SYM_HEIGHT	7
#define SYM_COUNT	(SYM_LAST - SYM_FIRST + 1)
#define MAX_LINE_LEN	81


static const int bit_mapping[SYM_HEIGHT] = { 0, 1, 3, 4, 5, 6, 7 };


static bool read_font_line(char *line, FILE *ifile) {
	int lnlen, i;

	// read line
	// while ignoring empty lines and comments
	do {
		if (!fgets(line, MAX_LINE_LEN, ifile)) {
			return false;
		}
	} while (*line == '\n' || *line == '\r' || *line == '-');

	// check contents
	lnlen = strlen(line);
	if (lnlen < (SYM_WIDTH + 1)) {
		fprintf(stderr, "Too short line!\n");
		return false;
	} else if (line[lnlen-1] != '\n') {
		fprintf(stderr, "Too long line or missing newline at the end of file!\n");
		return false;
	}
	for (i = 0; i < lnlen; ++i) {
		if ((i  < SYM_WIDTH && line[i] != ' ' && line[i] != '#') ||
		    (i >= SYM_WIDTH && !isspace(line[i]))) {
			fprintf(stderr, "Invalid line contents!\n");
			return false;
		}
	}

	return true;
}

static bool load_sym(uint8_t *bitmap, FILE *ifile) {
	char line[MAX_LINE_LEN];
	uint8_t x, y;

	memset(bitmap, 0, SYM_WIDTH);

	for (y = 0; y < SYM_HEIGHT; ++y) {
		if (!read_font_line(line, ifile)) {
			return false;
		}

		for (x = 0; x < SYM_WIDTH; ++x) {
			if (line[x] == '#') {
				bitmap[x] |= 1 << bit_mapping[y];
			}
		}
	}

	return true;
}

static uint8_t* load_font(FILE *ifile) {
	uint8_t *font;
	uint8_t sym;
	bool is_ok = true;

	font = (uint8_t*)malloc(SYM_WIDTH * SYM_COUNT);

	for (sym = SYM_FIRST; is_ok && sym <= SYM_LAST; ++sym) {
		uint8_t *sym_bitmap = &font[(sym - SYM_FIRST) * SYM_WIDTH];
		is_ok = load_sym(sym_bitmap, ifile);
	}

	if (is_ok) {
		return font;
	} else {
		free(font);
		return NULL;
	}
}

int write_font(const uint8_t *font, FILE *ofile) {
	char desc_buf[16];
	uint8_t y, sym;

	fprintf(ofile, "#include \"font%dx%d.h\"\n", SYM_WIDTH, SYM_HEIGHT);
	fprintf(ofile, "\n");
	fprintf(ofile, "// defines ASCII characters 0x%02X-0x%02X (%d-%d)\n", SYM_FIRST, SYM_LAST, SYM_FIRST, SYM_LAST);
	fprintf(ofile, "const uint8_t PROGMEM font%dx%d[] = {\n", SYM_WIDTH, SYM_HEIGHT);
	for (sym = SYM_FIRST; sym <= SYM_LAST; ++sym) {
		fprintf(ofile, "\t");
		for (y = 0; y < SYM_WIDTH; ++y) {
			fprintf(ofile, "0x%02X, ", *(font++));
		}
		switch (sym) {
			case ' ': strcpy(desc_buf, "(space)"); break;
			case '\\': strcpy(desc_buf, "\"\\\""); break;
			case 127: strcpy(desc_buf, "[]"); break;
			default: sprintf(desc_buf, "%c", sym); break;
		}
		fprintf(ofile, "// %s\n", desc_buf);
	}
	fprintf(ofile, "};\n");
	return 0;
}

int main(int argc, char **argv) {
	FILE *ifile, *ofile;
	uint8_t *font;
	int ret = 1;

	if (argc != 3) {
		fprintf(stderr, "Usage: fontconv infile.txt outfile.c\n");
		return 1;
	}

	ifile = fopen(argv[1], "rt");
	if (!ifile) {
		fprintf(stderr, "Can't open input file!\n");
		return 1;
	}

	ofile = fopen(argv[2], "wt");
	if (!ofile) {
		fprintf(stderr, "Can't open output file!\n");
		return 1;
	}

	font = load_font(ifile);
	if (font) {
		ret = write_font(font, ofile);
		free(font);
	}

	fclose(ofile);
	fclose(ifile);

	return ret;
}
