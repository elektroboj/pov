#include <stdbool.h>
#include <stdint.h>

#include "adc-hw.h"
#include "ticks.h"
#include "adc.h"


static uint16_t sense_avg;
static uint16_t sense_max;
static uint16_t sense_min;
static uint16_t sense_prev;
static uint16_t delta_trig;
static uint16_t delta_start;
static uint16_t adc_ticks;
static uint16_t sync_ticks;
static uint16_t sync_ticks_prev;
static enum {
	STATE_RISING,
	STATE_WAIT_BOT,
	STATE_SYNC,
	STATE_FALLING,
} state;


static void extremes_reset(void) {
	sense_max = 0;
	sense_min = UINT16_MAX;
}

void adc_init(void) {
	adc_hw_init();
	extremes_reset();
}

static void handle_extremes(void) {
	if (adc_ticks % 1024 == 0) {
		delta_trig = (sense_max - sense_min) * 1 / 4;
		extremes_reset();
	}

	if (sense_avg > sense_max) {
		sense_max = sense_avg;
	}
	if (sense_avg < sense_min) {
		sense_min = sense_avg;
	}
}

// NOTE: returns avg*4
static uint16_t calc_avg(uint16_t prev, uint16_t new) {
	return (prev * 3 + 2) / 4 + new;
}

static void update_sync_ticks(void) {
	sync_ticks = adc_ticks - sync_ticks_prev;
	sync_ticks_prev = adc_ticks;
}

void adc_reset_timeout(void) {
	sync_ticks_prev = adc_ticks;
}

static bool is_sync_timeout(void) {
	// 4096 ticks is a bit less than 0.5s
	return (adc_ticks - sync_ticks_prev) > 4096;
}

static void handle_trace(void) {
	switch (state) {
		default:
		case STATE_RISING:
			if (sense_prev > sense_avg) {
				state = STATE_FALLING;
				delta_start = sense_avg;
			}
			break;

		case STATE_FALLING:
			if (sense_prev < sense_avg) {
				state = STATE_RISING;
			} else if ((delta_start - sense_avg) > delta_trig) {
				state = STATE_SYNC;
			}
			break;

		case STATE_WAIT_BOT:
			if (sense_prev < sense_avg) {
				state = STATE_RISING;
			}
			break;

		case STATE_SYNC:
			state = STATE_WAIT_BOT;
			update_sync_ticks();
			break;
	}
}

void adc_on_measure(uint16_t adc_val) __attribute__((weak));
void adc_on_measure(uint16_t adc_val) {
	sense_avg = calc_avg(sense_avg, adc_val);
	++adc_ticks;
	handle_extremes();
	handle_trace();
	sense_prev = sense_avg;
}

adc_sync_t adc_detect_sync(void) {
	uint16_t adc_val;
	if (adc_hw_measure(&adc_val)) {
		adc_on_measure(adc_val);

		if (is_sync_timeout()) {
			return ADC_SYNC_TIMEOUT;
		} else if (state == STATE_SYNC) {
			return ADC_SYNC_NOW;
		}
	}

	return ADC_SYNC_NONE;
}

uint16_t adc_get_sync_ticks(void) {
	return sync_ticks * 13;
}

// adc is a stable tick provider
// other sources are either too fast (ir) or with varying speed (display)
uint16_t get_ticks(void) {
	return adc_ticks;
}
