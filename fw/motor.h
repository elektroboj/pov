#ifndef _MOTOR_H_
#define _MOTOR_H_

#include <stdbool.h>

void motor_init(void);
void motor_set(bool enable);

#endif
