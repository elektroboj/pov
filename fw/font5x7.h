#ifndef _FONT5X7_H_
#define _FONT5X7_H_

#include <stdint.h>

#include <avr/pgmspace.h>


extern const uint8_t PROGMEM font5x7[];

#endif
