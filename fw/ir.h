#ifndef _IR_H_
#define _IR_H_

#define KEY_0		'0'
#define KEY_1		'1'
#define KEY_2		'2'
#define KEY_3		'3'
#define KEY_4		'4'
#define KEY_5		'5'
#define KEY_6		'6'
#define KEY_7		'7'
#define KEY_8		'8'
#define KEY_9		'9'
#define KEY_STAR	'*'
#define KEY_HASH	'#'
#define KEY_OK		'\n'
#define KEY_UP		'U'
#define KEY_DOWN	'D'
#define KEY_LEFT	'L'
#define KEY_RIGHT	'R'
#define KEY_NONE	'\0'

void ir_start(void);
char ir_read(void);

#endif
