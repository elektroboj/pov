#ifndef _IDLER_H_
#define _IDLER_H_

#include <stdbool.h>

void idler_init(void);
bool idler_task(void);

#endif
