#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>

#include "display.h"
#include "motor.h"
#include "adc.h"
#include "ir.h"


int main() {
	display_init();
	motor_init();
	adc_init();
	ir_start();
	sei();

	for (;;) {
		static bool motor_enabled = false;
		char key = ir_read();
		if (key == '*') {
			motor_enabled = !motor_enabled;
			motor_set(motor_enabled);
		}

		if (adc_detect_sync()) {
			static uint8_t sync_id = 0;
			++sync_id;
			display_bitmap(sync_id);
			if (sync_id >= 6) {
				sync_id = 0;
			}
		}
	}
}
