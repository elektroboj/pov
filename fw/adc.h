#ifndef _ADC_H_
#define _ADC_H_

#include <stdint.h>


typedef enum {
	ADC_SYNC_NONE = 0,
	ADC_SYNC_NOW,
	ADC_SYNC_TIMEOUT,
} adc_sync_t;


void adc_init(void);
void adc_reset_timeout(void);
adc_sync_t adc_detect_sync(void);
uint16_t adc_get_sync_ticks(void);

#endif
