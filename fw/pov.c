#include <stdint.h>

#include <avr/interrupt.h>

#include "display.h"
#include "motor.h"
#include "idler.h"
#include "word.h"
#include "adc.h"
#include "ir.h"


static void chill(void) {
	display_stop();
	motor_set(false);
	idler_init();
	while (idler_task()) {
		adc_detect_sync();
	}
}

static void run(void) {
	motor_set(true);
	display_start();
	word_init();
	adc_reset_timeout();
	while (word_task()) {
		switch (adc_detect_sync()) {
			case ADC_SYNC_NOW:
				display_sync(adc_get_sync_ticks());
				break;

			case ADC_SYNC_TIMEOUT:
				goto abort;
				break;

			// ignored
			case ADC_SYNC_NONE:
				break;
		}
	}
abort:;
}

int main() {
	display_init();
	motor_init();
	adc_init();
	ir_start();
	sei();

	for (;;) {
		chill();
		run();
	}
}
