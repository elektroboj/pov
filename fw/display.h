#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <stdint.h>

#include "fastfunc.h"


#define DISPLAY_PIX_WIDTH	256
#define DISPLAY_MAX_CHARS	(DISPLAY_PIX_WIDTH / 2 / 8)


void display_init(void);
void display_start(void);
void display_stop(void);
void display_rotate(int8_t steps);
void display_bitmap(uint8_t bitmap);
void display_sync(uint16_t sync_ticks);
void FASTFUNC display_clear(void);
void display_setc(uint8_t pos, char c);
void display_sets(uint8_t pos, const char *s);


#endif
