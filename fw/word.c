#include <stdbool.h>
#include <stdint.h>

#include <avr/eeprom.h>

#include "display.h"
#include "ticks.h"
#include "word.h"
#include "ir.h"


#define MAX_MESSAGES 10


static bool is_editing;
static uint8_t messages[MAX_MESSAGES][DISPLAY_MAX_CHARS] EEMEM = {
//   0123456789012345
	"Elektroboj 2019 ",	// 1
	" Hello world!!! ",	// 2
	"   Work hard!   ",	// 3
	"  Sleep tight!  ",	// 4
	"    Be happy!   ",	// 5
	"Have a nice day ",	// 6
	"  Summer 2019   ",	// 7
	"eHacktIt 1.6.   ",	// 8
	"42              ",	// 9
	":)  :D  ;)  :P  ",	// 10
};
static uint8_t msg_id;
static uint8_t edit_ofs;
static uint8_t edit_ascii;
static uint16_t blink_ts;
static bool blink_cursor;
static int8_t roll_speed;


static inline char get_message_char(uint8_t ofs) {
	return (char)eeprom_read_byte(&messages[msg_id][ofs]);
}

static inline char get_message_cur_char(void) {
	return get_message_char(edit_ofs);
}

static void switch_message(uint8_t new_msg_id) {
	uint8_t i;

	if (new_msg_id >= MAX_MESSAGES) {
		return;
	}

	msg_id = new_msg_id;
	for (i = 0; i < DISPLAY_MAX_CHARS; ++i) {
		const char c = get_message_char(i);
		display_setc(i, c);
	}
}

void word_init(void) {
	// hacky compile-time check if messages fits to EEPROM
	switch(0) { case 0: case sizeof(messages) <= 256:;}

	is_editing = false;
	roll_speed = 0;
	switch_message(0);
}

static void cursor_show(void) {
	blink_cursor = true;
	display_setc(edit_ofs, '\x7F');
	blink_ts = get_ticks();
}

static void cursor_hide(void) {
	blink_cursor = false;
	display_setc(edit_ofs, get_message_cur_char());
	blink_ts = get_ticks();
}

static void cursor_blink(void) {
	if (get_ticks() - blink_ts >= MS_TO_TICKS(500)) {
		if (!blink_cursor) {
			cursor_show();
		} else {
			cursor_hide();
		}
	}
}

static void edit_enter(void) {
	edit_ofs = 0;
	edit_ascii = 0;
	cursor_show();
	is_editing = true;
}

static void edit_leave(void) {
	cursor_hide();
	is_editing = false;
}

static void store_char(char c) {
	const uint8_t uc = (uint8_t)c;
	if (uc < ' ' || uc >= 128) {
		return;
	}

	display_setc(edit_ofs, c);
	eeprom_update_byte(&messages[msg_id][edit_ofs], uc);
}

static void edit_move_ofs(int8_t dir) {
	int8_t new_ofs;

	if (eeprom_is_ready()) {
		cursor_hide();
	}

	new_ofs = (int8_t)edit_ofs + dir;
	if (new_ofs < 0) {
		new_ofs = DISPLAY_MAX_CHARS - 1;
	} else if (new_ofs >= DISPLAY_MAX_CHARS) {
		new_ofs = 0;
	}
	edit_ofs = new_ofs;
	cursor_show();
}

static void edit_digit(uint8_t digit) {
	edit_ascii = edit_ascii * 10 + digit;

	if (edit_ascii >= 32) {
		store_char((char)edit_ascii);
		edit_move_ofs(1);
		edit_ascii = 0;
	}
}

static void roll(void) {
	uint16_t delay;

	switch (roll_speed) {
		case -1: case 1: delay = MS_TO_TICKS(100); break;
		case -2: case 2: delay = MS_TO_TICKS(64);  break;
		case -3: case 3: delay = MS_TO_TICKS(32);  break;
		default: return;
	}

	if (get_ticks() - blink_ts >= delay) {
		blink_ts = get_ticks();
		display_rotate(roll_speed > 0 ? 1 : -1);
	}
}

static void presenter_task(char key) {
	switch (key) {
		case KEY_UP:
			switch_message(msg_id - 1);
			break;

		case KEY_DOWN:
			switch_message(msg_id + 1);
			break;

		case '0' ... '9':
		{
			uint8_t new_msg_id = key - '0';
			if (new_msg_id == 0) new_msg_id = 10;
			switch_message(new_msg_id - 1);
			break;
		}

		case KEY_RIGHT:
			if (roll_speed < 3) roll_speed += 1;
			break;

		case KEY_LEFT:
			if (roll_speed > -3) roll_speed -= 1;
			break;

		case KEY_OK:
			edit_enter();
			break;
	}

	roll();
}

static void editor_task(char key) {
	if (!eeprom_is_ready()) {
		return;
	}

	switch (key) {
		case KEY_UP:
			store_char(get_message_cur_char() + 1);
			edit_ascii = 0;
			break;

		case KEY_DOWN:
			store_char(get_message_cur_char() - 1);
			edit_ascii = 0;
			break;

		case KEY_LEFT:
			edit_move_ofs(-1);
			edit_ascii = 0;
			break;

		case KEY_RIGHT:
			edit_move_ofs(+1);
			edit_ascii = 0;
			break;

		case '0' ... '9':
			edit_digit(key - '0');
			break;

		case KEY_OK:
			edit_leave();
			break;
	}

	cursor_blink();
}

bool word_task(void) {
	char key = ir_read();

	if (is_editing) {
		editor_task(key);
	} else {
		presenter_task(key);
	}

	return key != '*';
}
