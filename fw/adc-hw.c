#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>

#include "adc-hw.h"


void adc_hw_init(void) {
	DIDR0 = _BV(ADC2D) | _BV(ADC1D);
	ADMUX = (2 << REFS0) | (2 << MUX0);
	ADCSRA = _BV(ADEN) | _BV(ADATE) | (6 << ADPS0);
	ADCSRA |= _BV(ADSC);
}

bool adc_hw_measure(uint16_t *val) {
	if (ADCSRA & _BV(ADIF)) {
		*val = ADC;
		ADCSRA |= _BV(ADIF);
		return true;
	}
	return false;
}
