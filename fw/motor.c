#include <avr/io.h>

#include "motor.h"


void motor_init(void) {
	DDRB |= _BV(PB0);
}

void motor_set(bool enable) {
	if (enable) {
		PORTB |=  _BV(PB0);
	} else {
		PORTB &= ~_BV(PB0);
	}
}
