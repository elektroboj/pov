#ifndef _WORD_H_
#define _WORD_H_

#include <stdbool.h>

void word_init(void);
bool word_task(void);

#endif
