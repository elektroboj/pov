#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <util/delay.h>

#include "display.h"
#include "motor.h"
#include "adc.h"


static void strobe_out(bool is_hi, uint8_t val) {
	val = (val & 0x1F) | (is_hi ? 0x20 : 0x00);
	display_bitmap(0x00 | val);
	_delay_us(2);
	display_bitmap(0x40 | val);
	_delay_us(2);
}

// this function will override internal adc.c function
void adc_on_measure(uint16_t val) {
	strobe_out(false, val);
	strobe_out(true, val >> 5);
}

int main() {
	display_init();
	motor_init();
	adc_init();
	sei();

	motor_set(true);

	for (;;) {
		adc_detect_sync();
	}
}
