#include <avr/interrupt.h>
#include <util/delay.h>

#include "display.h"
#include "ir.h"


int main() {
	display_init();
	ir_start();
	sei();

	for (;;) {
		static char prev_key;
		char key = ir_read();
		if (key) {
			if (prev_key == key) {
				for (char i = 0; i < 5; ++i) {
					display_clear();
					_delay_ms(100);
					display_bitmap(key);
					_delay_ms(100);
				}
			}

			display_bitmap(key);
			prev_key = key;
		}
	}
}
