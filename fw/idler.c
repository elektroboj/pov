#include <stdbool.h>

#include "display.h"
#include "idler.h"
#include "ticks.h"
#include "ir.h"


static struct {
	bool shr : 1;
	uint8_t bitmask : 7;
} state;
static uint16_t prev_ticks;


void idler_init(void) {
	state.bitmask = 0x01;
}

bool idler_task(void) {
	if ((get_ticks() - prev_ticks) >= 800) {
		prev_ticks = get_ticks();

		// check bounds
		if (state.bitmask == 0x01) {
			state.shr = false;
		} else if (state.bitmask == 0x40) {
			state.shr = true;
		}

		// shift
		if (state.shr) {
			state.bitmask >>= 1;
		} else {
			state.bitmask <<= 1;
		}

		// display
		display_bitmap(state.bitmask);
	}

	return ir_read() != '*';
}
