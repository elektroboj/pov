#include <string.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "fastfunc.h"
#include "display.h"
#include "font5x7.h"


#define DISPLAY_TIM_PRESCALER	1
#define DISPLAY_TICK_DIV	(DISPLAY_PIX_WIDTH * DISPLAY_TIM_PRESCALER / 64)


static uint16_t sync_ticks_sum;
static volatile uint8_t pix_id;
static uint8_t buffer[DISPLAY_MAX_CHARS];
static uint8_t rotation;


void display_init(void) {
	DDRA |= _BV(PA0) | _BV(PA1) | _BV(PA3) | _BV(PA4) | _BV(PA5) | _BV(PA6) | _BV(PA7);
}

void display_start(void) {
	TCCR1A = 0;
	TCCR1B = _BV(WGM12) | (1 << CS10);	// CTC mode, /1
	TIMSK1 = _BV(OCIE1A);

	rotation = 0;
	memset(buffer, 0, DISPLAY_MAX_CHARS);
}

void display_stop(void) {
	TIMSK1 &= ~_BV(OCIE1A);
}

void display_rotate(int8_t steps) {
	rotation -= steps;
}

static void FASTFUNC display_raw(uint8_t raw) {
	PORTA = raw & ~_BV(PA2);
}

void display_bitmap(uint8_t bitmap) {
	const uint8_t bithi = (bitmap << 1) & 0xF8;
	const uint8_t bitlo = bitmap & 0x03;
	display_raw(bithi | bitlo);
}

void display_clear(void) {
	display_raw(0x00);
}

void display_setc(uint8_t pos, char c) {
	uint8_t uc = c;
	if (pos < DISPLAY_MAX_CHARS) {
		if (uc < ' ' || uc == 0xFF) {
			uc = 0;
		} else if (uc >= 128) {
			uc = '?';
		} else {
			uc -= ' ';
		}
		buffer[pos] = uc;
	}
}

void display_sets(uint8_t pos, const char *s) {
	char c;
	for (; (c = pgm_read_byte(s)); ++s, ++pos) {
		display_setc(pos, c);
	}
}

static void start_new_frame() {
	TIMSK1 &= ~_BV(OCIE1A);
	display_clear();
	pix_id = 0;
	TCNT1 = 0;
	TIMSK1 |= _BV(OCIE1A);
}

/*
 * Empyrical data:
 *	About 96 ADC ticks is 1/6 turn
 *	1 ADC tick is 13*64 CPU ticks
 *  About 79872 CPU ticks is 1/6 turn
 *  1/6 turn contains 42.666 pix
 *  Therefore:
 *		One pix is about 1902 CPU ticks
 *
 * Math:
 *  display_cpu_ticks = sync_ticks * 64 * 6
 *  pix_cpu_ticks = display_cpu_ticks / DISPLAY_PIX_WIDTH
 *  pix_cpu_ticks = sync_ticks * 6 / (DISPLAY_PIX_WIDTH / 64)
 *  pix_tmr_ticks = pix_cpu_ticks / pix_tmr_prescaler
 *  OCRnA = pix_tmr_ticks - 1
 *
 */

void display_sync(uint16_t sync_ticks) {
	static uint8_t sync_id;
	sync_ticks_sum += sync_ticks;
	if (++sync_id >= 6) {
		sync_id = 0;
		OCR1A = (sync_ticks_sum + DISPLAY_TICK_DIV / 2) / DISPLAY_TICK_DIV - 1;
		sync_ticks_sum = 0;
		start_new_frame();
	}
}

#pragma GCC push_options
#pragma GCC optimize ("O2")
static void display_font_column(uint8_t c, uint8_t char_bit) {
	const uint16_t font_ofs = c * 5 + char_bit;
	const uint8_t font_column = pgm_read_byte(&font5x7[font_ofs]);
	display_raw(font_column);
}

ISR(TIM1_COMPA_vect) {
	if (pix_id % 2 == 0) {
		const uint8_t id = (pix_id / 2 + rotation) % 128;
		const uint8_t char_id = id / 8;
		const uint8_t char_bit = id % 8;

		if (char_bit < 5) {
			display_font_column(buffer[char_id], char_bit);
		} else {
			display_clear();
		}
	} else {
		display_clear();
	}

	++pix_id;
	if (pix_id == 0) {
		TIMSK1 &= ~_BV(OCIE1A);
	}
}
#pragma GCC pop_options
