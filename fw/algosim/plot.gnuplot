#run with: gnuplot -e fname='"input.txt"' -p plot.gnuplot

if (!exists("fname")) exit message "fname not defined"

set y2range [0:3]
set y2tics ("rising" 0, "wait-bot" 1, "sync" 2, "falling" 3)
set ytics nomirror
plot fname using 1 title "raw" with lines, \
     fname using 2 title "avg" with lines, \
	 fname using 3 title "max" with fsteps, \
	 fname using 4 title "min" with fsteps, \
	 fname using 5 axis x1y2 title "state"
