# Algorithm simulator

This environment helps with testing of the synchronization algorithm.

## Creating input files

 * burn the test-adcread firmware
 * connect PulseView to LED1..LED7
   * configure to 50M samples and 2MHz bandwidth
   * add `Parallel` protocol decoder
     * configure LED7 as clock
 * turn on the power
   * motor should be free!
   * motor will start running as soon as the power is turned on
   * parallel stream from ADC will also start
 * start recording in PulseView
   * you can add a load to the motor if required
   * a slight touch with a finger should suffice to simulate actual load
 * wait for recording to finish
 * right click to Parallel data stream "export all annotations for this row"
   * save this data stream as `*.txt`
 * optionally compress the data stream using `gzip`.


## Running the simulator

### Basic run

`./algosim infile.txt > outfile.txt`

This will take an ADC stream (`infile.txt`),
run the motor synchronization algorithm,
and output the internal values and states in `outfile.txt`

### Graphing

ADC stream and algorithm output can be visualized with:

`./plot.sh infile`

`infile` is ADC stream in either compressed or uncompressed format.

You can use `bad-motor.txt.gz` can be used as an example.

### Counting errors

`./count_errors.sh` will count errors in every `m*.txt.gz` and `m*.txt` file
in the current working directory.

Optionally, input files can be specified via command line arguments.
