#/!bin/bash
set -e
root=`dirname $0`
infile="$1"

if [ -z "$infile" ]
then
	echo "Usage: $0 infile" >&2
	exit 1
fi

# create data file
outroot=/tmp/pov-algosim-plot
mkdir -p "$outroot"
out="$outroot/out-`basename "$infile"`"
if [ "${infile##*.}" = gz ]
then
	gunzip -c "$infile" | "$root"/algosim -
else
	"$root"/algosim "$1"
fi > "$out"

# plot!
gnuplot -p -e fname="\"$out\"" "$root"/plot.gnuplot

# cleanup
# none! gnuplot exits immediately, and expects the input file to still exist!
