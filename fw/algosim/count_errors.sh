#!/bin/bash
set -e

root=`dirname $0`
if [ -z "$1" ]
then
	shopt -s nullglob
	infiles=(m*.txt *.txt.gz)
	shopt -u nullglob
else
	infiles=("$@")
fi

for fname in "${infiles[@]}"
do
	echo -n "$fname errors: " ;
	if [ "${fname##*.}" = gz ]
	then
		gunzip -c "$fname"
	else
		cat "$fname"
	fi | "$root"/algosim - 2>&1 | grep Missed\ sync | wc -l
done
