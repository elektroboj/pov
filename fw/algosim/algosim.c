#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>


#define IS_HI(x) (!!((x) & 0x20))

// This is ugly, but enables access to private (static) variables
#include "../adc.c"


static FILE *f_in;
static uint16_t sense_raw;
static bool done = false;


bool rdhex(FILE *fi, uint8_t *val) {
	char s[128];
	if (!fgets(s, sizeof(s), fi))
		return false;
	return sscanf(s, "%*s %*s %*s %"SCNx8, val) == 1;
}

bool rdval(FILE *fi, uint16_t *val) {
	static bool init = true;
	uint8_t lo, hi;

	// read low
	if (!rdhex(fi, &lo))
		return false;
	// it is ok to read again while at the beginning
	if (init && IS_HI(lo) && !rdhex(fi, &lo))
		return false;
	init = false;
	if (IS_HI(lo)) {
		fprintf(stderr, "High after high?!\n");
		return false;
	}

	// read high
	if (!rdhex(fi, &hi))
		return false;
	if (!IS_HI(hi)) {
		fprintf(stderr, "Low after low?!\n");
		return false;
	}

	*val = ((hi & 0x1F) << 5) | lo;
	return true;
}

void adc_hw_init(void) {
}

bool adc_hw_measure(uint16_t *val) {
	done = !rdval(f_in, val);
	sense_raw = *val;
	return true;
}

int main(int argc, char *argv[]) {
	unsigned int sync_id = 0;
	unsigned int data_id;
	uint16_t prev_sync_ticks = 0;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s pulseview-infile.txt\n", *argv);
		return 1;
	}

	if (!strcmp(argv[1], "-")) {
		f_in = stdin;
	} else {
		f_in = fopen(argv[1], "rt");
		if (!f_in) {
			fprintf(stderr, "Can't open %s!\n", argv[1]);
			return 1;
		}
	}

	adc_init();
	for (data_id = 0; !done; ++data_id) {
		adc_sync_t sync = adc_detect_sync();
		if (sync == ADC_SYNC_NOW) {
			if (delta_trig > 0) {
				uint16_t cur_sync_ticks = adc_get_sync_ticks();
				if (prev_sync_ticks > 0 && cur_sync_ticks > prev_sync_ticks * 3 / 2) {
					fprintf(stderr, "Missed sync #%u (@%u)\n", sync_id, data_id);
				}
				prev_sync_ticks = cur_sync_ticks;
			}
			++sync_id;
		}
		printf("%u %u %u %u %u\n", sense_raw, sense_avg, sense_max, sense_min, state);
	}
	fclose(f_in);
	return 0;
}
