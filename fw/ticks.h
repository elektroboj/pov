#ifndef _TICKS_H_
#define _TICKS_H_

#include <stdint.h>

#define MS_TO_TICKS(ms)	(ms * (F_CPU / 64 / 13) / 1000)

uint16_t get_ticks(void);

#endif
