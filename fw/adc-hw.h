#ifndef _ADC_HW_H_
#define _ADC_HW_H_

#include <stdbool.h>
#include <stdint.h>

void adc_hw_init(void);
bool adc_hw_measure(uint16_t *val);

#endif
